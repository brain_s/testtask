<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 13.12.17
 * Time: 15:43
 */

namespace App\Controllers;

use \Core\View;
use \App\Models\Notifications;
class Notification extends \Core\Controller
{

    /**w
     * @param string $name
     * @param int $uid
     */

    public function activationAction($name,$uid){
        $data = Notifications::setActivation($name,$uid);

    }

    /**
     * @param string $name
     * @param int $uid
     * @param float $amount
     */
    public function billingAction($name,$uid,$amount){
        $data = Notifications::setBilling($name,$uid,$amount);
    }

    /**
     * @param string $name
     * @param int $uid
     */
    public function stopAction($name,$uid){

        $data = Notifications::getStop($name,$uid);
    }

}