<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 15.12.17
 * Time: 10:30
 */

namespace App\Controllers;

use \Core\View;
use \Core\Controller;
use \App\Models\Service;

class Services extends Controller
{

    /**
     * @param string $name
     */
    public function indexAction($name){
        $data = Service::getOne($name);
        View::renderTemplate('Service/index.php',array('data'=>$data,'post' => $_REQUEST));
    }
    public function allAction(){
        $data = Service::getAll();
        View::renderTemplate('Service/all.php',array('data'=>$data));
    }

    public function deleteAction($id){

    }

    public function editAction($id){
        print_r($id);
        $data = Service::getAll();
        View::renderTemplate('Service/all.php',array('data'=>$data));
    }

    public function addAction(){

    }


}
