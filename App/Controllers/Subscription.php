<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 13.12.17
 * Time: 9:52
 */

namespace App\Controllers;

use \Core\View;
use \App\Models\Subs;

class Subscription extends \Core\Controller
{

    public function indexAction()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_REQUEST['token'] !='' && $_REQUEST['uid'] !=''){
            $request=$_REQUEST;
            $data=Subs::getAll();

            View::renderTemplate('Subscription/index.php',array('data'=>$data));
        }
    }

    public function allAction()
    {
            $data=Subs::getAll();

            View::renderTemplate('Subscription/all.php',array('data'=>$data));
    }

    public function newAction()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_REQUEST['token'] !='' && $_REQUEST['uid'] !='') {

            $data = Subs::createSubs($_REQUEST['token'],$_REQUEST['uid']);
        }
    }

    public function checkAction()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_REQUEST['token'] !='' && $_REQUEST['uid'] !='') {
            $data = Subs::checkSubs($_REQUEST['token'],$_REQUEST['uid']);

        }
    }

    public function stopAction()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_REQUEST['token'] !='' && $_REQUEST['uid'] !='') {

            $data = Subs::stopSubs($_REQUEST['token'],$_REQUEST['uid']);

        }
    }

    public function profileAction()
    {

            View::renderTemplate('Subscription/profile.php', array('amount' =>111));
    }
}