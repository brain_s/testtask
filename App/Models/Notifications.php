<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 13.12.17
 * Time: 15:37
 */

namespace App\Models;

use PDO;

class Notifications extends \Core\Model
{

    public static function setActivation($name,$uid)
    {
        $db = static::getDB();
        $stmt = $db->prepare('UPDATE subscriptions SET status=? WHERE sid=? AND uid=?');
        $stmt->bindParam(1, $status);
        $stmt->bindParam(2, $sid);
        $stmt->bindParam(3, $uid);
        $row = $db->query( "SELECT `id` FROM `services` WHERE `name`='$name'" )->fetch();
        $sid=intval($row['id']);
        $status=1;
        $stmt->execute();
    }
    public static function setBilling($name,$uid,$amount)
    {
        $db = static::getDB();
        $stmt = $db->prepare('UPDATE subscriptions SET billing=? WHERE sid=? AND uid=? AND status=?');
        $stmt->bindParam(1, $biling);
        $stmt->bindParam(2, $sid);
        $stmt->bindParam(3, $uid);
        $stmt->bindParam(4, $status);
        $row = $db->query( "SELECT `id` FROM `services` WHERE `name`='$name'" )->fetch();
        $sid=intval($row['id']);
        $biling=$amount;
        $stmt->execute();
    }
    public static function getStop($name,$uid)
    {
        $db = static::getDB();
        $stmt = $db->prepare('UPDATE `subscriptions` SET `status`=? WHERE uid=? AND sid=?');
        $stmt->bindParam(1, $status);
        $stmt->bindParam(2, $uid);
        $stmt->bindParam(3, $sid);
        $row = $db->query( "SELECT `id` FROM `services` WHERE `name`='$name'" )->fetch();
        $sid=intval($row['id']);
        $status=0;
        $stmt->execute();
    }

}