<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 14.12.17
 * Time: 15:22
 */

namespace App\Models;

use PDO;

class Service extends \Core\Model
{
    /**
     * @return mixed
     */
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT * FROM services');
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getOne($name)
    {
        $db = static::getDB();
        $stmt = $db->prepare('SELECT * FROM services WHERE `name`=?');
        $stmt->bindParam(1, $name);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

}