<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 13.12.17
 * Time: 9:52
 */

namespace App\Models;

use PDO;
use DateTime;

class Subs extends \Core\Model
{
    public static function getAll(){

        $db = static::getDB();
        $stmt = $db->query('SELECT * FROM subscriptions');
        $fetch=$stmt->fetchAll(PDO::FETCH_ASSOC);
        $json=json_encode($fetch);
        return $fetch;
    }


    public static function createSubs($token,$uid){

        $db = static::getDB();
        $stmt = $db->prepare('INSERT INTO `subscriptions`(`sid`, `uid`, `from`, `to`, `status`, `billing`) VALUES (?,?,?,?,?,?)');
        $stmt->bindParam(1, $sid);
        $stmt->bindParam(2, $uid);
        $stmt->bindParam(3, $from);
        $stmt->bindParam(4, $to);
        $stmt->bindParam(5, $status);
        $stmt->bindParam(6, $billing);

        /**
         *  insert one row
         */
        $row = $db->query( "select id from services WHERE token='$token'" )->fetch();
        $sid=intval($row['id']);
        $from=null;
        $to=null;
        $status=0;
        $billing=100;
        $stmt->execute();
    }

    /**
     * @param string $token
     * @param integer $uid
     */
    public static function checkSubs($token,$uid){
        $db = static::getDB();
        $stmt = $db->prepare('UPDATE `subscriptions` SET `from`=?, `to`=? WHERE  uid=? AND status=? AND sid=?');
        $stmt->bindParam(1, $from);
        $stmt->bindParam(2, $to);
        $stmt->bindParam(3, $uid);
        $stmt->bindParam(4, $status);
        $stmt->bindParam(5, $sid);
        $date=new DateTime('now');
        $from=$date->format('Y-m-d H:i:s');
        $date->modify('+1 month');
        $to=$date->format('Y-m-d H:i:s');
        $row = $db->query( "select id from services WHERE token='$token'" )->fetch();
        $sid=intval($row['id']);
        $uid=intval($uid);
        $status=1;
        $stmt->execute();

    }

    /**
     * @param string $token
     * @param integer $uid
     */
    public static function stopSubs($token,$uid){
        $db = static::getDB();
        $stmt = $db->prepare('UPDATE `subscriptions` SET `status`=? WHERE uid=? AND sid=?');
        $stmt->bindParam(1, $status);
        $stmt->bindParam(2, $uid);
        $stmt->bindParam(3, $sid);
        $row = $db->query( "select id from services WHERE token='$token'" )->fetch();
        $sid=intval($row['id']);
        $status=0;
        $stmt->execute();
    }

}