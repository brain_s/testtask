{% extends "base.html" %}

{% block title %}Service{% endblock %}

{% block body %}

    {% for d in data %}
        <h1>Service name: {{ d.name }}</h1>
        {% if d.status == false %}
            <p>Status: offline</p>
        {% else %}
            <p>Status: active</p>
        {% endif %}
    <form id="Subscription" metod="POST" action="#">
        <input type="text" value="3" name="uid" hidden>
        <input type="text" value="{{d.token}}" name="token" hidden>
        <input id="sub" type="submit" value="Subscribe"></input>
    </form>

    <form id="Login" metod="POST" action="#">
        <input type="text" value="3" name="uid" hidden>
        <input type="text" value="{{d.token}}" name="token" hidden>
        <input id="chek" type="submit" value="Check"></input>
    </form>

    <form id="Unsubscription" metod="post" action="#">
        <input type="text" value="3" name="uid" hidden>
        <input type="text" value="{{d.token}}" name="token" hidden>
        <input id="unsub" type="submit" value="Unsubscribe"></input>
    </form>
    {% endfor %}

{% endblock %}
