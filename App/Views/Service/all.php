{% extends "base.html" %}

{% block title %}Service{% endblock %}

{% block body %}


<table border="1" cellspacing="0" cellpadding="5">
    <th>id</th>
    <th>name</th>
    <th>status</th>
    <th colspan="2">Action</th>
    <tbody>
    {% for d in data %}
        <tr>
            <td>{{d.id}}</td>
            <td>{{ d.name }}</td>
            <td>{% if d.status==1 %} active {% else %} offline{% endif%}</td>
            <td width="40" align="left" ><a href="#" onClick="show_confirm('edit',{{d.id}})">Edit</a></td>
            <td width="40" align="left" ><a href="#" onClick="show_confirm('delete',{{d.id}})">Delete </a></td>
        </tr>
    {% endfor %}
    <tr>
        <td colspan="7" align="right"> <a href="add">Insert New User</a></td>
    </tr>

    </tbody>
</table>
<script type="text/javascript">
    function show_confirm(act,gotoid)
    {
        if(act=="edit")
        var r=confirm("Do you really want to edit?");
    else
        var r=confirm("Do you really want to delete?");
        if (r==true)
        {
            location.href =act+"/"+gotoid;

        }
    }
</script>


{% endblock %}