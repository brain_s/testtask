{% extends "base.html" %}
{% block body %}

<table id="demo" cellspacing="2" border="1" cellpadding="5">
    <thead>
    <tr>
        <th>UID</th>
        <th>FROM</th>
        <th>TO</th>
        <th>STATUS</th>
        <th>STOP</th>
    </tr>
    </thead>
    <tbody>
    {% for d in data %}
    <tr>
        <td>{{d.uid}}</td>
        <td>{{d.from}}</td>
        <td>{{d.to}}</td>
        <td>{{d.status}}</td>
        <td><button id="stop" type="button">Stop</button></td>
    </tr>
    {% endfor %}
    </tbody>
</table>

{% endblock %}