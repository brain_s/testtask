<?php

/**
 * Front controller
 *
 * PHP version 7.0
 */

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';


/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');


/**
 * Routing
 */
$router = new Core\Router();

// Add the routes
$router->add('', ['controller' => 'Home', 'action' => 'index']);
$router->add('{service}', ['controller' => 'Services', 'action' => 'index']);
$router->add('{controller}/{action}');
$router->add('{controller}/{action}/');
$router->add('{controller}/{action}/{id:\d+}');
$router->add('{controller}/{action}/{name}');
$router->add('{controller}/{action}/{name}/{uid:\d+}');
$router->add('{controller}/{action}/{name}/{uid:\d+}/{amount:\d+}');
    
$router->dispatch($_SERVER['QUERY_STRING']);
