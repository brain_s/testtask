$(function () {

    $( "#sub" ).click(function() {
        $.post( "subscription/new", {
            uid: document.forms[0][0].value,
            token: document.forms[0][1].value
        } );
    });
    $( "#chek" ).click(function() {
        $.post( "subscription/check", {
            uid: document.forms[1][0].value,
            token: document.forms[1][1].value
        }, function() {
            location.href = 'subscription/profile';
        });
    });
    $( "#unsub" ).click(function() {
        $.post( "subscription/stop", {
            uid: document.forms[2][0].value,
            token: document.forms[2][1].value
        } );
    });

})